# PeakOTron

A tool to perform automated fits of Silicon Photo-Multiplier charge spectra to extract useful parameters from experiments. PeakOTron can be used for detailed analyses of single spectra, and also for the automatic characterisation of large samples of SiPMs.

The starting values of the fit parameters are extracted from the charge spectra.
The entire data, including the intervals in-between the photoelectron peaks, are fitted to determine parameters such as the mean number of detected photons, gain, gain spread, prompt cross-talk, pedestal, electronics noise, dark-count rate as well as probability and time constant of after-pulses. 



To get started, have a look at ```example.py```.


## References

Jack Rolph, Erika Garutti, Robert Klanner, Tobias Quadfasel, Jörn Schwandt: _PeakOTron: A Python module for fitting charge spectra of Silicon Photomultipliers_.  [doi.org/10.1016/j.nima.2023.168544](https://doi.org/10.1016/j.nima.2023.168544)